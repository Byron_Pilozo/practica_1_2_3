package com.example.byronpilozo.practica_1_2_3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametro extends AppCompatActivity {
    EditText CajaDatos;
    Button BTNenviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        CajaDatos = (EditText)findViewById((R.id.txtParametro));
        BTNenviar=(Button)findViewById((R.id.BTNEnviarParametro));
        BTNenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PasarParametro.this,ActividadRecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",CajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}
